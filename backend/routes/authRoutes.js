const express = require('express');
const {Router} = express;
const router = new Router();
const {
  registerUser,
  loginUser,
  restoreUserPass} = require('../controllers/authController');

router.route('/register').post(registerUser);
router.route('/login').post(loginUser);
router.route('/forgot_password').post(restoreUserPass);

module.exports = router;
