const fs = require('fs');
const path = require('path');
const express = require('express');
// eslint-disable-next-line no-unused-vars
const dotenv = require('dotenv').config();
const {errorHandler} = require('./middleware/errorMiddleware');
const connectDB = require('./config/db');
const port = process.env.PORT || 8080;
const cors = require('cors');
const morgan = require('morgan');
const accessLogStream = fs.createWriteStream(
    path.join(__dirname, 'logs.log'), {flags: 'a'});

connectDB();

const app = express();
app.use(cors());
app.use(morgan('combined', {stream: accessLogStream}));
app.use(morgan('tiny'));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use('/api/trucks', require('./routes/truckRoutes'));
app.use('/api/loads', require('./routes/loadRoutes'));
app.use('/api/users/me', require('./routes/userRoutes'));
app.use('/api/auth', require('./routes/authRoutes'));

app.use(errorHandler);

app.listen(port, () => console.log(`server runs on port ${port}`));
