/* eslint-disable camelcase */
const asyncHadler = require('express-async-handler');
const User = require('../models/userModel');
const Truck = require('../models/truckModel');
const Load = require('../models/loadModel');
const bcrypt = require('bcryptjs');
const isDriverBusy = require('./utils/DriverUtils');

// @desc Get user info
// @route GET /api/users/me
// @access Private
const getMyInfo = asyncHadler(async (req, res) => {
  if (!req.user) {
    res.status(400);
    throw new Error('User doesn`t exist');
  } else {
    const {_id, role, email, created_date} = await User.findById(req.user.id);
    res.status(200).json({
      user: {
        _id,
        role,
        email,
        created_date,
      }});
  };
});

// @desc Delete user profile
// @route DELETE /api/users/me
// @access Private
const deleteMe = asyncHadler(async (req, res) => {
  if (!req.user) {
    res.status(400);
    throw new Error('User doesn`t exist');
  } else {
    const user = await User.findById(req.user.id);
    const _id = user._id;
    if (req.user.role === 'DRIVER' && await isDriverBusy(user)) {
      res.status(400);
      throw new Error('Cannot delete your account while on load');
    } else if (req.user.role === 'SHIPPER' &&
     await (await Load.find({created_by: _id, status: 'ASSIGNED'})).length) {
      res.status(400);
      throw new Error('Cannot delete your account while you have active loads');
    } else {
      await User.deleteOne({_id});
      await Truck.deleteMany({created_by: _id});
      await Load.deleteMany({created_by: _id});
      res.status(200).json({message: 'Profile deleted successfully'});
    }
  }
});

// @desc Change password
// @route PATCH /api/users/me/password
// @access Private
const changeMyPass = asyncHadler(async (req, res) => {
  if (!req.user) {
    res.status(400);
    throw new Error('User doesn`t exist');
  } else {
    if (req.user.role === 'DRIVER' && await isDriverBusy(req.user)) {
      res.status(400);
      throw new Error('Cannot change your password while on load');
    }
    const {oldPassword, newPassword} = req.body;
    const user = await User.findById(req.user.id);
    const oldpasswordDB = user.password;
    if (!(await bcrypt.compare(oldPassword, oldpasswordDB))) {
      res.status(400);
      throw new Error('Wrong old password');
    } else if (!newPassword) {
      res.status(400);
      throw new Error('New password should not be empty');
    } else if (oldPassword === newPassword) {
      res.status(400);
      throw new Error('New password should be different from old one');
    } else {
      const salt = await bcrypt.genSalt(10);
      const hashPassword = await bcrypt.hash(newPassword, salt);
      await User.findOneAndUpdate({_id: req.user.id}, {password: hashPassword});
      res.status(200).json({message: 'Password changed successfully'});
    }
  }
});


module.exports = {
  getMyInfo,
  deleteMe,
  changeMyPass,
};
