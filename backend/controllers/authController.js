const asyncHadler = require('express-async-handler');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const User = require('../models/userModel');
const nodemailer = require('nodemailer');
const passGenerator = require('generate-password');
// @desc Register user
// @route POST /api/auth/register
// @access Public
const registerUser = asyncHadler(async (req, res) => {
  const {email, password, role} = req.body;
  if (!email || !password || !role) {
    res.status(400);
    throw new Error('Please check all fields');
  }
  const mailReg = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/;
  if (!mailReg.test(email)) {
    res.status(400);
    throw new Error(`Incorrect email format`);
  }
  const userExists = await User.findOne({email: email});
  if (userExists) {
    res.status(400);
    throw new Error(`User with email '${email}' already exists`);
  }
  const salt = await bcrypt.genSalt(10);
  const hashPassword = await bcrypt.hash(password, salt);
  const user = await User.create({
    email: email,
    password: hashPassword,
    role: role,
  });
  if (user) {
    res.status(200).json({
      message: 'Profile created successfully',
      jwt_token: generateToken(user._id),
    });
  } else {
    res.status(400);
    throw new Error('Invalid user data');
  }
});

// @desc Login user
// @route POST /api/auth/login
// @access Public
const loginUser = asyncHadler(async (req, res) => {
  const {email, password} = req.body;
  const user = await User.findOne({email});
  if (user && ( await bcrypt.compare(password, user.password))) {
    res.status(200).json({
      message: 'Success',
      username: user.username,
      jwt_token: generateToken(user._id),
    });
  } else {
    res.status(400);
    throw new Error('Invalid user credentials');
  }
});

// @desc Send password to user email
// @route POST /api/auth/forgot_password
// @access Public
const restoreUserPass = asyncHadler(async (req, res) => {
  const {email} = req.body;
  const user = await User.findOne({email});
  if (user) {
    const password = passGenerator.generate({
      length: 10,
      numbers: true,
    });
    const salt = await bcrypt.genSalt(10);
    const hashPassword = await bcrypt.hash(password, salt);
    try {
      sendPassToMail(user.email, password);
    } catch (err) {
      res.status(500).json({message: err.message});
    }
    user.password = hashPassword;
    await user.save();
    res.json({
      message: 'New password sent to your email address',
    });
  } else {
    res.status(400);
    throw new Error('User with such email not found');
  }
});


const generateToken = (id) => {
  return jwt.sign({id}, process.env.JWT_SECRET, {
    expiresIn: '10d',
  });
};

const sendPassToMail = (userMail, userPass) => {
  const transporter = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    port: 465,
    secure: true,
    auth: {
      user: process.env.NOTIFY_MAIL_USERNAME,
      pass: process.env.NOTIFY_MAIL_PASS,
    },
  });
  const mailOptions = {
    from: 'Delivery API',
    to: userMail,
    subject: 'Forget password',
    text: `Your password from delivery app is: ${userPass}`,
  };
  transporter.sendMail(mailOptions, (err) => {
    console.log(err.message);
  });
};

module.exports = {
  registerUser,
  loginUser,
  restoreUserPass,
};
