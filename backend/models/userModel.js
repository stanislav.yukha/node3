const mongoose = require('mongoose');
const {Schema} = mongoose;
const userSchema = new Schema({
  password: {
    type: String,
    required: [true, 'Please provide password'],
  },
  role: {
    type: String,
    enum: ['SHIPPER', 'DRIVER'],
    default: 'SHIPPER',
    required: true,
  },
  email: {
    type: String,
    required: [true, 'Please provide email'],
  },
}, {
  timestamps: {createdAt: 'created_date', updatedAt: false},
},
);

module.exports = mongoose.model('User', userSchema);
