const mongoose = require('mongoose');
const {Schema} = mongoose;
const truckSchema = new Schema({
  created_by: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'User',
  },
  assigned_to: {
    type: mongoose.Schema.Types.Mixed,
    default: null,
  },
  type: {
    type: String,
    enum: ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'],
    default: 'SPRINTER',
    required: true,
  },
  status: {
    type: String,
    enum: ['OL', 'IS'],
    default: 'IS',
    required: true,
  },
}, {
  timestamps: {createdAt: 'created_date', updatedAt: false},
},
);

module.exports = mongoose.model('Truck', truckSchema);
