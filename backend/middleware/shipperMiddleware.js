const asyncHadler = require('express-async-handler');
const protectShipper = asyncHadler(async (req, res, next) => {
  const role = req.user.role;
  if (role === 'SHIPPER') {
    next();
  } else {
    res.status(400);
    throw new Error('Allowed only for shipper role');
  }
});

module.exports = protectShipper;
