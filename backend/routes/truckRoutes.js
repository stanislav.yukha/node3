const express = require('express');
const {Router} = express;
const router = new Router();
const {
  getTrucks,
  addTruck,
  getTruck,
  updateTruck,
  deleteTruck,
  assignTruck} = require('../controllers/truckController');
const protect = require('../middleware/authMiddleware');
const protectDriver = require('../middleware/driverMiddleware');

router.route('/')
    .get(protect, protectDriver, getTrucks)
    .post(protect, protectDriver, addTruck);
router.route('/:id')
    .get(protect, protectDriver, getTruck)
    .put(protect, protectDriver, updateTruck)
    .delete(protect, protectDriver, deleteTruck);

router.route('/:id/assign').post(protect, protectDriver, assignTruck);

module.exports = router;
