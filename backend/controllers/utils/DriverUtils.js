const Truck = require('../../models/truckModel');

module.exports = async function isDriverBusy(user) {
  const truck = await Truck.findOne({assigned_to: user._id, status: 'OL'});
  return !!truck;
};

