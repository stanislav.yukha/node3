const asyncHadler = require('express-async-handler');
const Truck = require('../models/truckModel');
const isDriverBusy = require('./utils/DriverUtils');

// @desc Get trucks
// @route GET /api/trucks
// @access Private
const getTrucks = asyncHadler(async (req, res) => {
  const trucks = await Truck.find({created_by: req.user.id});
  res.status(200).json({trucks});
});

// @desc Add truck
// @route POST /api/trucks
// @access Private
const addTruck = asyncHadler(async (req, res) => {
  if (!req.body.type) {
    res.status(400);
    throw new Error('No \'type\' parameter provided or it is empty');
  }
  await Truck.create({
    created_by: req.user._id,
    type: req.body.type,
  });
  res.status(200).json({message: 'Truck created successfully'});
});

// @desc Get truck by id
// @route GET /api/trucks/:id
// @access Private
const getTruck = asyncHadler(async (req, res) => {
  const truck = await Truck.findOne(
      {_id: req.params.id,
        created_by: req.user._id,
      });
  if (!truck) {
    res.status(400);
    throw new Error('Truck with such id is not available for you');
  }
  res.status(200).json({truck});
});

// @desc Update truck by id
// @route PUT /api/trucks/:id
// @access Private
const updateTruck = asyncHadler(async (req, res) => {
  if (await isDriverBusy(req.user)) {
    res.status(400);
    throw new Error('You cannot update trucks info while on load');
  }
  const truck = await Truck.findOne(
      {_id: req.params.id, created_by: req.user._id});
  if (!truck) {
    res.status(400);
    throw new Error(`Truck with such id is not found`);
  }
  console.log(truck);
  if (truck.assigned_to !== null) {
    res.status(400);
    throw new Error(`You cannot update assigned truck`);
  }
  if (!req.body.type) {
    res.status(400);
    throw new Error(`Parameter 'type' should not be empty`);
  }
  truck.type = req.body.type;
  try {
    await truck.save();
  } catch (err) {
    res.status(400);
    throw new Error(`Wrong 'type' parameter value`);
  }
  res.status(200).json({
    message: 'Truck details changed successfully',
  } );
});

// @desc Delete truck by id
// @route DELETE /api/trucks/:id
// @access Private
const deleteTruck = asyncHadler(async (req, res) => {
  if (await isDriverBusy(req.user)) {
    res.status(400);
    throw new Error('You cannot delete trucks info while on load');
  }
  const truck = await Truck.findOne(
      {_id: req.params.id, created_by: req.user._id});
  if (!truck) {
    res.status(400);
    throw new Error(`Truck with such id is not found`);
  }
  if (truck.assigned_to !== null) {
    res.status(400);
    throw new Error(`You cannot delete assigned truck`);
  }
  await Truck.deleteOne({_id: req.params.id});
  res.status(200).json({
    message: 'Truck deleted successfully'});
});

// @desc Assign truck by id to current driver
// @route POST /api/trucks/:id/assign
// @access Private
const assignTruck = asyncHadler(async (req, res) => {
  if (await isDriverBusy(req.user)) {
    res.status(400);
    throw new Error('You cannot reassign truck while on load');
  }
  const newTruck = await Truck.findOne(
      {_id: req.params.id, created_by: req.user._id});
  if (!newTruck) {
    res.status(400);
    throw new Error(`Truck with such id is not found`);
  }

  const currentTruck = await Truck.findOne(
      {created_by: req.user._id, assigned_to: req.user._id});
  if (currentTruck) {
    currentTruck.assigned_to = null;
    await currentTruck.save();
  }

  newTruck.assigned_to = req.user._id;
  await newTruck.save();
  res.status(200).json({
    message: 'Truck assigned successfully',
  });
});


module.exports = {
  getTrucks,
  addTruck,
  getTruck,
  updateTruck,
  deleteTruck,
  assignTruck,
};

