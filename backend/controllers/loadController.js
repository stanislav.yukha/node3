const asyncHadler = require('express-async-handler');
const Load = require('../models/loadModel');
const User = require('../models/userModel');
const Truck = require('../models/truckModel');

const loadStates = [
  'En route to Pick Up',
  'Arrived to Pick Up',
  'En route to delivery',
  'Arrived to delivery'];


const truckSizes = {
  'SPRINTER': {
    width: 250,
    length: 300,
    height: 170,
    payload: 1700,
  },
  'SM STRAIGHT': {
    width: 250,
    length: 500,
    height: 170,
    payload: 2500,
  },
  'LG STRAIGHT': {
    width: 350,
    length: 700,
    height: 200,
    payload: 4000,
  },
};


// @desc Get loads
// @route GET /api/loads
// @access Private
const getLoads = asyncHadler(async (req, res) => {
  const {offset, limit, status} = req.query;
  let loads;
  if (req.user.role === 'DRIVER') {
    const statusList = ['NEW', 'POSTED'];
    if (status) {
      status === 'ASSIGNED' ?
       statusList.push('SHIPPED') :
       statusList.push('ASSIGNED');
    }
    loads = await Load.find(
        {assigned_to: req.user._id,
          status: {$nin: statusList}})
        .skip(offset || 0)
        .limit(limit || 10);
  } else {
    const findQuery = status ?
    {created_by: req.user._id, status: status} :
    {created_by: req.user._id};
    loads = await Load.find(
        findQuery)
        .skip(offset || 0)
        .limit(limit || 10);
  }
  console.log(loads);
  res.status(200).json({loads});
});

// @desc Add load
// @route POST /api/loads
// @access Private
const addLoad = asyncHadler(async (req, res) => {
  if (!req.body.name ||
    !req.body.payload ||
    !req.body.pickup_address ||
    !req.body.delivery_address ||
    !req.body.dimensions) {
    res.status(400);
    throw new Error('Not all parameters provided in request body');
  }
  await Load.create({
    name: req.body.name,
    created_by: req.user.id,
    payload: req.body.payload,
    pickup_address: req.body.pickup_address,
    delivery_address: req.body.delivery_address,
    dimensions: {
      width: req.body.dimensions.width,
      length: req.body.dimensions.length,
      height: req.body.dimensions.height,
    },
    logs: [
      {
        message: 'Newly created load, waiting for posting',
        time: new Date(),
      },
    ],

  });
  res.status(200).json({message: 'Load created successfully'});
});

// @desc Get active load for driver
// @route GET /api/loads/active
// @access Private
const getActiveLoad = asyncHadler(async (req, res) => {
  const load = await Load.findOne({
    assigned_to: req.user._id,
    status: 'ASSIGNED',
  });
  if (!load) {
    res.status(400);
    throw new Error('No active load for this driver found');
  }
  res.status(200).json({load});
});

// @desc Update load by id
// @route PATCH /api/loads/active/state
// @access Private
const changeActiveLoadState = asyncHadler(async (req, res) => {
  const load = await Load.findOne({
    status: 'ASSIGNED',
    assigned_to: req.user._id,
  });
  if (!load) {
    res.status(400);
    throw new Error('No active load for this driver found');
  }
  const index = loadStates.indexOf(load.state) + 1;
  load.state = loadStates[index];
  if (load.state === 'Arrived to delivery') {
    load.status = 'SHIPPED';
    const truck = await Truck.findOne({assigned_to: req.user._id});
    truck.status = 'IS';
    await truck.save();
  }
  load.logs.push({
    message: `Load state changed to ${loadStates[index]}`,
    time: new Date(),
  });
  await load.save();
  res.status(200).json({
    message: `Load state changed to ${loadStates[index]}`,
  });
});

// @desc Get load by id
// @route GET /api/loads/:id
// @access Private
const getLoad = asyncHadler(async (req, res) => {
  if (!req.params.id) {
    res.status(400);
    throw new Error('No load \'id\' parameter provided');
  }
  let load;
  if (req.user.role === 'DRIVER') {
    load = await Load.findOne({_id: req.params.id, assigned_to: req.user._id});
  } else {
    load = await Load.findOne({_id: req.params.id, created_by: req.user._id});
  }
  if (!load) {
    res.status(400);
    throw new Error('No load with such id found for this user');
  }
  res.status(200).json({load});
});

// @desc update load by id
// @route PUT /api/loads/:id
// @access Private
const updateLoad = asyncHadler(async (req, res) => {
  if (!req.params.id) {
    res.status(400);
    throw new Error('No load \'id\' parameter provided');
  }
  const load = await Load
      .findOne({_id: req.params.id, created_by: req.user._id});
  if (!load) {
    res.status(400);
    throw new Error('No load with such id found for this user');
  }
  console.log(load);
  if (load.status !== 'NEW') {
    res.status(400);
    throw new Error(`You cannot update load with status ${load.status}`);
  } else {
    await Load.findOneAndUpdate(
        {
          _id: req.params.id,
          created_by: req.user._id,
        },
        req.body);
    res.status(200).json({message: 'Load details changed successfully'});
  }
});


// @desc Delete load by id
// @route DELETE /api/loads/:id
// @access Private
const deleteLoad = asyncHadler(async (req, res) => {
  if (!req.params.id) {
    res.status(400);
    throw new Error('No load \'id\' parameter provided');
  }
  const load = await Load
      .findOne({_id: req.params.id, created_by: req.user._id});
  if (!load) {
    res.status(400);
    throw new Error('No load with such id found');
  }
  if (load.status !== 'NEW') {
    res.status(400);
    throw new Error(`You cannot delete load with status ${load.status}`);
  } else {
    await Load.deleteOne({_id: req.params.id, created_by: req.user._id});
    res.status(200).json({message: 'Load deleted successfully'});
  }
});

// @desc post load by id
// @route DELETE /api/loads/:id
// @access Private
const postLoad = asyncHadler(async (req, res) => {
  if (!req.params.id) {
    res.status(400);
    throw new Error('No load \'id\' parameter provided');
  }
  const load = await Load
      .findOne({_id: req.params.id, created_by: req.user._id});
  if (!load) {
    res.status(400);
    throw new Error('No load with such id found');
  }
  if (load.status !== 'NEW') {
    res.status(400);
    throw new Error('You cannot post load with status other than NEW');
  }
  load.status = 'POSTED';
  load.logs.push({
    message: `Load is posted`,
    time: new Date(),
  });
  await load.save();
  const {width, height, length} = load.dimensions;
  const {payload} = load;
  let suitableTruckType;
  if (width <= truckSizes.SPRINTER.width &&
    height <= truckSizes.SPRINTER.width &&
    length <= truckSizes.SPRINTER.length &&
    payload <= truckSizes.SPRINTER.payload) {
    suitableTruckType = ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'];
  } else if (width <= truckSizes['SM STRAIGHT'].width &&
    height <= truckSizes['SM STRAIGHT'].width &&
    length <= truckSizes['SM STRAIGHT'].length &&
    payload <= truckSizes['SM STRAIGHT'].payload) {
    suitableTruckType = ['SMALL STRAIGHT', 'LARGE STRAIGHT'];
  } else if (width <= truckSizes['LG STRAIGHT'].width &&
    height <= truckSizes['LG STRAIGHT'].width &&
    length <= truckSizes['LG STRAIGHT'].length &&
    payload <= truckSizes['LG STRAIGHT'].payload) {
    suitableTruckType = ['LARGE STRAIGHT'];
  } else {
    load.status = 'NEW';
    load.logs.push({
      message: `No truck found, load status switched back to 'NEW'`,
      time: new Date(),
    });
    await load.save();
    return res.status(200)
        .json({message: 'Sorry, no truck found for such load dimensions'});
  }
  let truck;
  let driver;
  const alltrucks = await Truck.find({status: 'IS', assigned_to: {$ne: null}});
  (function() {
    let trucks;
    for (let k = 0; k < suitableTruckType.length; k++) {
      trucks = alltrucks.filter((truck) => truck.type === suitableTruckType[k]);
      if (trucks.length) {
        truck = trucks[0];
        return;
      }
    }
  })();

  if (truck) {
    driver = await User.findById(truck.assigned_to);
  };

  if (!driver) {
    load.status = 'NEW';
    load.logs.push({
      message: `Load status switched back to NEW, no driver found`,
      time: new Date(),
    });
    await load.save();
    res.status(200).json({
      message: 'Load is not posted',
      driver_found: false,
    });
  } else {
    truck.status = 'OL';
    await truck.save();
    load.state = loadStates[0];
    load.assigned_to = driver._id;
    load.status = 'ASSIGNED';
    load.logs.push({
      message: `Load assigned to driver with id ${driver._id}`,
      time: new Date(),
    });
    await load.save();
    res.status(200).json({
      message: 'Load posted successfully',
      driver_found: true,
    });
  }
});

// @desc Delete load by id
// @route GET /api/loads/:id/shipping_info
// @access Private
const getFullLoadInfo = asyncHadler(async (req, res) => {
  if (!req.params.id) {
    res.status(400);
    throw new Error('No load \'id\' parameter provided');
  }
  const load = await Load.find({_id: req.query.id, created_by: req.user._id});
  if (!load) {
    res.status(400);
    throw new Error('No load with such id found for this user');
  }
  const truck = await Truck.find({assigned_to: load.assigned_to});
  response.status(200).json({load, truck});
});


module.exports = {
  getLoads,
  addLoad,
  getActiveLoad,
  changeActiveLoadState,
  getLoad,
  updateLoad,
  deleteLoad,
  postLoad,
  getFullLoadInfo,
};

