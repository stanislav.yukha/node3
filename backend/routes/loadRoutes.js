const express = require('express');
const {Router} = express;
const router = new Router();
const {
  getLoads,
  addLoad,
  getActiveLoad,
  changeActiveLoadState,
  getLoad,
  updateLoad,
  deleteLoad,
  postLoad,
  getFullLoadInfo} = require('../controllers/loadController');
const protect = require('../middleware/authMiddleware');
const protectDriver = require('../middleware/driverMiddleware');
const protectShipper = require('../middleware/shipperMiddleware');

router.route('/')
    .get(protect, getLoads)
    .post(protect, protectShipper, addLoad);
router.route('/active').get(protect, protectDriver, getActiveLoad);
router.route('/active/state')
    .patch(protect, protectDriver, changeActiveLoadState);
router.route('/:id')
    .get(protect, getLoad)
    .put(protect, protectShipper, updateLoad)
    .delete(protect, protectShipper, deleteLoad);
router.route('/:id/post').post(protect, protectShipper, postLoad);
router.route('/:id/shipping_info')
    .get(protect, protectShipper, getFullLoadInfo);


module.exports = router;
