const asyncHadler = require('express-async-handler');
const protectDriver = asyncHadler(async (req, res, next) => {
  const role = req.user.role;
  if (role === 'DRIVER') {
    next();
  } else {
    res.status(400);
    throw new Error('Allowed only for driver role');
  }
});

module.exports = protectDriver;
