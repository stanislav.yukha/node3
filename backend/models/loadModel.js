const mongoose = require('mongoose');
const {Schema} = mongoose;
const loadSchema = new Schema({
  created_by: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'User',
  },
  assigned_to: {
    type: mongoose.Schema.Types.Mixed,
    default: null,
  },
  status: {
    type: String,
    enum: ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'],
    default: 'NEW',
  },
  state: {
    type: String,
    enum: [
      '',
      'En route to Pick Up',
      'Arrived to Pick Up',
      'En route to delivery',
      'Arrived to delivery'],
    default: '',
  },
  name: {
    type: String,
    required: true,
  },
  payload: {
    type: Number,
    required: true,
  },
  pickup_address: {
    type: String,
    required: true,
  },
  delivery_address: {
    type: String,
    required: true,
  },
  dimensions: {
    type: mongoose.SchemaTypes.Mixed,
    required: true,
  },
  logs: [
    {
      message: {
        type: String,
        required: false,
      },
      time: {
        type: Date,
        required: false,
      },
      _id: false,
    },
  ],
}, {
  timestamps: {createdAt: 'created_date', updatedAt: false},
},
);

module.exports = mongoose.model('Load', loadSchema);
